# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sistema_bibliotecario', '0002_acceso_autor_categoria_disponibilidad_editorial_estadolibro_estadoprestamo_libro_libroautor_persona_'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subcategoria',
            fields=[
                ('idsubcategoria', models.AutoField(serialize=False, primary_key=True)),
                ('nombresubcategoria', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'db_table': 'subcategoria',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Tipolibro',
            fields=[
                ('idtipolibro', models.AutoField(serialize=False, primary_key=True)),
                ('nombretipo', models.CharField(max_length=200)),
            ],
            options={
                'db_table': 'tipolibro',
                'managed': False,
            },
        ),
    ]
