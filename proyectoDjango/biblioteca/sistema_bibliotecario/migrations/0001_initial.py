# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Accesos',
            fields=[
                ('idacceso', models.IntegerField(serialize=False, primary_key=True)),
                ('clave', models.CharField(max_length=200)),
                ('fechaingreso', models.DateField()),
            ],
            options={
                'db_table': 'accesos',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Libros',
            fields=[
                ('id_libro', models.IntegerField(serialize=False, primary_key=True)),
                ('titulo', models.CharField(max_length=200)),
                ('editorial', models.CharField(max_length=200)),
                ('edicion', models.CharField(max_length=200)),
                ('fechapublicacion', models.DateField()),
                ('categoria', models.CharField(max_length=200)),
                ('autor', models.CharField(max_length=200)),
                ('ubicacion', models.CharField(max_length=200)),
                ('tipo', models.CharField(max_length=200)),
                ('idioma', models.CharField(max_length=200)),
                ('estado', models.CharField(max_length=200)),
                ('disponibilidad', models.CharField(max_length=200)),
            ],
            options={
                'db_table': 'libros',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Personas',
            fields=[
                ('idpersona', models.IntegerField(serialize=False, primary_key=True)),
                ('nombres', models.CharField(max_length=200)),
                ('apellidos', models.CharField(max_length=200)),
                ('telefono', models.CharField(max_length=200, null=True, blank=True)),
                ('direccion', models.CharField(max_length=200, null=True, blank=True)),
                ('correo', models.CharField(max_length=200, null=True, blank=True)),
                ('rol', models.CharField(max_length=200)),
            ],
            options={
                'db_table': 'personas',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Prestamos',
            fields=[
                ('idprestamo', models.IntegerField(serialize=False, primary_key=True)),
                ('fechaprestamo', models.DateField()),
                ('fechadevolucion', models.DateField()),
                ('estado', models.CharField(max_length=200)),
            ],
            options={
                'db_table': 'prestamos',
                'managed': False,
            },
        ),
    ]
