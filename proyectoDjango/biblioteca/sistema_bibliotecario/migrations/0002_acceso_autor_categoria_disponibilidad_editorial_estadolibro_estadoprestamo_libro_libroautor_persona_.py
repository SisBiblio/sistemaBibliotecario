# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sistema_bibliotecario', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Autor',
            fields=[
                ('id_autor', models.IntegerField(serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'db_table': 'autor',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('tipo', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'db_table': 'categoria',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Disponibilidad',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('disponibilidad', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'db_table': 'disponibilidad',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Editorial',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'db_table': 'editorial',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Estadolibro',
            fields=[
                ('estado', models.IntegerField(serialize=False, primary_key=True)),
                ('estado_1', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'db_table': 'estadolibro',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Libroautor',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('id_libro', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'db_table': 'libroautor',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('cedula', models.IntegerField(serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=100, null=True, blank=True)),
                ('apellido', models.CharField(max_length=100, null=True, blank=True)),
                ('telefono', models.CharField(max_length=50, null=True, blank=True)),
                ('direccion', models.CharField(max_length=200, null=True, blank=True)),
                ('correo', models.CharField(max_length=200, null=True, blank=True)),
                ('fechanacimiento', models.DateField(null=True, blank=True)),
            ],
            options={
                'db_table': 'persona',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Rol',
            fields=[
                ('idrol', models.IntegerField(serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'db_table': 'rol',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Sanciones',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fechasancion', models.DateField(null=True, blank=True)),
                ('motivo', models.CharField(max_length=200, null=True, blank=True)),
                ('diassancion', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'sanciones',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Acceso',
            fields=[
                ('cedula', models.ForeignKey(primary_key=True, db_column='cedula', serialize=False, to='sistema_bibliotecario.Persona')),
                ('idpersona', models.CharField(max_length=200, null=True, blank=True)),
                ('password', models.CharField(max_length=200, null=True, blank=True)),
                ('fechaingreso', models.DateField()),
            ],
            options={
                'db_table': 'acceso',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Libro',
            fields=[
                ('id', models.ForeignKey(primary_key=True, db_column='id', serialize=False, to='sistema_bibliotecario.Editorial')),
                ('id_editoriales', models.CharField(max_length=200, null=True, blank=True)),
                ('edicion', models.CharField(max_length=200, null=True, blank=True)),
                ('fechapublicacion', models.CharField(max_length=200, null=True, blank=True)),
                ('id_categoria', models.CharField(max_length=200, null=True, blank=True)),
                ('id_autor', models.CharField(max_length=200, null=True, blank=True)),
                ('ubicacion', models.CharField(max_length=200, null=True, blank=True)),
                ('tipo', models.CharField(max_length=200, null=True, blank=True)),
                ('idioma', models.CharField(max_length=200, null=True, blank=True)),
                ('id_estado', models.CharField(max_length=200, null=True, blank=True)),
                ('id_disponibilidad', models.CharField(max_length=200, null=True, blank=True)),
                ('titulo', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'db_table': 'libro',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Prestamo',
            fields=[
                ('cedula', models.ForeignKey(primary_key=True, db_column='cedula', serialize=False, to='sistema_bibliotecario.Libro')),
                ('id_persona', models.CharField(max_length=200)),
                ('id_libro', models.CharField(max_length=200)),
                ('fechaprestamo', models.DateField()),
                ('fechadevolucion', models.DateField()),
                ('idestado', models.CharField(max_length=200)),
            ],
            options={
                'db_table': 'prestamo',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Estadoprestamo',
            fields=[
                ('cedula', models.ForeignKey(primary_key=True, db_column='cedula', serialize=False, to='sistema_bibliotecario.Prestamo')),
                ('nombre', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'db_table': 'estadoprestamo',
                'managed': False,
            },
        ),
    ]
