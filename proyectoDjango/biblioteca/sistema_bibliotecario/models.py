#-*- encoding:utf-8 -*-
# Create your models here.
# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class Acceso(models.Model):
    idacceso = models.AutoField(primary_key=True)
    cedula = models.ForeignKey('Persona', db_column='cedula')
    password = models.CharField(max_length=25, blank=True, null=True)
    fechaingreso = models.DateField()

    class Meta:
        managed = False
        db_table = 'acceso'


class Autor(models.Model):
    id_autor = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=200, blank=True, null=True)
    apellido = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'autor'
    def __unicode__(self):
	return '%s %s' %(self.id_autor, self.nombre) 


class Categoria(models.Model):
    idcategoria = models.AutoField(primary_key=True)
    tipo = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'categoria'


class Disponibilidad(models.Model):
    iddisponibilidad = models.AutoField(primary_key=True)
    disponibilidad = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'disponibilidad'


class Editorial(models.Model):
    ideditorial = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=25, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'editorial'


class Libro(models.Model):
    idlibro = models.AutoField(primary_key=True)
    idsubcategoria = models.ForeignKey('Subcategoria', db_column='idsubcategoria')
    idtipolibro = models.ForeignKey('Tipolibro', db_column='idtipolibro')
    iddisponibilidad = models.ForeignKey(Disponibilidad, db_column='iddisponibilidad')
    ideditorial = models.ForeignKey(Editorial, db_column='ideditorial')
    edicion = models.CharField(max_length=200, blank=True, null=True)
    fechapublicacion = models.DateField(blank=True, null=True)
    ubicacion = models.CharField(max_length=200, blank=True, null=True)
    idioma = models.CharField(max_length=200, blank=True, null=True)
    titulo = models.CharField(max_length=200, blank=True, null=True)
    codigolibro = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'libro'
    def __unicode__(self):
        return '%s'%(self.titulo)


class Libroautor(models.Model):
    idlibroautor = models.AutoField(primary_key=True)
    id_1 = models.ForeignKey(Libro, db_column='id_1')
    id_autor = models.ForeignKey(Autor, db_column='id_autor')

    class Meta:
        managed = False
        db_table = 'libroautor'
    def __unicode__(self):
	return '%s %s %s' %(self.idlibroautor, self.id_1, self.id_autor)


class Persona(models.Model):
    cedula = models.IntegerField(primary_key=True)
    idrol = models.ForeignKey('Rol', db_column='idrol')
    nombre = models.CharField(max_length=200, blank=True, null=True)
    apellido = models.CharField(max_length=250, blank=True, null=True)
    telefono = models.CharField(max_length=150, blank=True, null=True)
    direccion = models.CharField(max_length=200, blank=True, null=True)
    correo = models.CharField(max_length=200, blank=True, null=True)
    fechanacimiento = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'persona'


class Prestamo(models.Model):
    idprestamo = models.AutoField(primary_key=True)
    idlibro = models.ForeignKey(Libro, db_column='idlibro')
    cedula = models.ForeignKey(Persona, db_column='cedula')
    fechaprestamo = models.DateField()
    fechadevolucion = models.DateField()
    descripcion = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'prestamo'


class Rol(models.Model):
    idrol = models.AutoField(primary_key=True)
    nombrerol = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rol'


class Sanciones(models.Model):
    idsanciones = models.AutoField(primary_key=True)
    cedula = models.ForeignKey(Persona, db_column='cedula')
    fechasancion = models.DateField(blank=True, null=True)
    motivo = models.CharField(max_length=200, blank=True, null=True)
    diassancion = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sanciones'


class Subcategoria(models.Model):
    idsubcategoria = models.AutoField(primary_key=True)
    idcategoria = models.ForeignKey(Categoria, db_column='idcategoria')
    nombresubcategoria = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'subcategoria'


class Tipolibro(models.Model):
    idtipolibro = models.AutoField(primary_key=True)
    nombretipo = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'tipolibro'
