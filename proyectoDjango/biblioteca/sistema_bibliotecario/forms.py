from django import forms

class addUsuarioForm(forms.Form):

    # = models.ForeignKey('Rol', db_column='idrol')
	cedula= forms.IntegerField(widget=forms.TextInput(attrs={'placeholder':'Ingrese el numero de cedula'}))
	nombre = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Ingrese su nombre'}))
	apellido = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Ingrese su apellido'}))
	telefono = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Ingrese su telefono'}))
	direccion = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Ingrese su direccion'}))
	correo = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Ingrese su correo'}))
	fechanacimiento= forms.DateField(widget=forms.TextInput(attrs={'placeholder':'Ingrese la fecha de nacimiento'}))
	#idrol= forms.IntegerField(widget=forms.TextInput())

	def clean(self):
		return self.cleaned_data

class addMaterialForm(forms.Form):

    # = models.ForeignKey('Rol', db_column='idrol')
	codigo= forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Ingrese el codigo del material bibliografico'}))
	titulo = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Ingrese el titulo'}))
	idioma = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Ingrese el idioma'}))
	ubicacion = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Ingrese la ubicacion'}))
	fechapublicacion = forms.DateField(widget=forms.TextInput(attrs={'placeholder':'Ingrese fecha de publicacion'}))
	edicion = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Ingrese edicion'}))
	
	def clean(self):
		return self.cleaned_data
