from django.conf.urls import patterns, url

urlpatterns=patterns('sistema_bibliotecario.views', 
	url(r'^$','index_view',name='index'),
	#url(r'^editar$','contactos_m',name='contactos'), 
	url(r'^registrar$','registrar_u',name='registrar'), 
	url(r'^editar$','buscar',name='consultar_u'), 
	url(r'^modificar$','editar_m',name='editar_m'), 
	url(r'^consultar$','consultar_us',name='consultar_u'), 
	url(r'^i_material$','ingreso_material',name='ingreso_material'),
	url(r'^modificar_m$','modificar_material',name='modificar_material'), 
	url(r'^eliminar_m$','eliminar_material',name='eliminar_material'),  
    url(r'^buscar_m$','buscar_material',name='buscar_material'), 
	)
