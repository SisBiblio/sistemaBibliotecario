from django.contrib import admin
from sistema_bibliotecario.models import *
# Register your models here.
admin.site.register(Libro)
admin.site.register(Autor)
admin.site.register(Libroautor)
