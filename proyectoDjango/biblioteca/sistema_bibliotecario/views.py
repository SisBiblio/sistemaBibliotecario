from django.shortcuts import render, render_to_response, RequestContext
from django.db.models import Q
from sistema_bibliotecario.forms import addUsuarioForm
from sistema_bibliotecario.forms import addMaterialForm

from sistema_bibliotecario.models import Persona
from sistema_bibliotecario.models import Rol
from .models import *
from django.http import HttpResponse
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.http import JsonResponse
from django.template import RequestContext


# Create your views here.
def index_view(request):
    return render_to_response('index.html',context=RequestContext(request))

def contactos_m(request):
    return render_to_response('Registrar.html',context=RequestContext(request))

def registrar_u(request):

	if request.method == "POST":
		form = addUsuarioForm(request.POST)
		info = "Inicializando"
		if form.is_valid():
				rol = request.POST['q']
				idrol = Rol.objects.get(idrol=rol)
				#idrol = Rol.objects.get(idrol=1)
				#idrol=form.cleaned_data['idrol']
				cedula= form.cleaned_data['cedula']
				nombre = form.cleaned_data['nombre']
				apellido = form.cleaned_data['apellido']
				telefono = form.cleaned_data['telefono']
				direccion = form.cleaned_data['direccion']
				correo = form.cleaned_data['correo']
				fechanacimiento = form.cleaned_data['fechanacimiento']
				p = Persona()
				p.cedula=cedula
				p.idrol= idrol
				p.nombre=nombre
				p.apellido=apellido
				p.telefono=telefono
				p.direccion=direccion
				p.correo=correo
				p.fechanacimiento=fechanacimiento				
				p.save() #Guardar la informacion
				info="Se  registro correctamente !!"
		else:
				info="Debe llenar todos los campos"
		form = addUsuarioForm()
		ctx = {'form': form, 'informacion':info}
		rol= Rol.objects.all()
			#lista['titulo'] = "Listado de usuarios"
    		#lista['Persona'] = persona
    		ctx= {'form': form, 'rol':rol,'info':info}

    		return render_to_response('Registrar.html',ctx,context_instance=RequestContext(request))

	else: #GET
		form = addUsuarioForm()
		ctx = {'form': form}
		rol= Rol.objects.all()
			#lista['titulo'] = "Listado de usuarios"
    		#lista['Persona'] = persona
    		ctx= {'form': form, 'rol':rol}	

    	return render_to_response('Registrar.html',ctx,context_instance=RequestContext(request))

def registrar_usu(request):

	if request.method == "POST":
		nombres=request.POST['nombres']
		apellidos=request.POST['apellidos']
		correoelectronico=request.POST['correoelectronico']
		cedula=request.POST['direccion']
		direccion=request.POST['cedula']
		telefono=request.POST['telefono']
		fechanacimiento=request.POST['fechanacimiento']
		
		form = addUsuarioForm(request.POST)
		info = "Inicializando"
		if form.is_valid():
				
				idrol = Rol.objects.get(idrol=1)
				#idrol=form.cleaned_data['idrol']		
				p = Persona()
				p.cedula=cedula
				p.idrol=idrol
				p.nombre=nombres
				p.apellido=apellidos
				p.telefono=telefono
				p.direccion=direccion
				p.correo=correoelectronico
				p.fechanacimiento=fechanacimiento				
				p.save() #Guardar la informacion
				info="Se  registro correctamente !!"
		else:
				info="datos incorrectos"
		form = addUsuarioForm()
		ctx = {'form': form, 'informacion':info}

    		return render_to_response('Registrar.html',ctx,context_instance=RequestContext(request))

	else: #GET
		form = addUsuarioForm()
		ctx = {'form': form}

    	return render_to_response('Registrar.html',ctx,context_instance=RequestContext(request))



def editarss_m(request,id_p):
	#nombres = form.cleaned_data['nombres']
	p= Personas.objects.get(cedula=id_p)
	if request.method == "POST":
		form=addUsuarioForm(request.POST,request.FILES)
		if form.is_valid():
				
				nombres = form.cleaned_data['nombres']
				apellidos = form.cleaned_data['apellidos']
				telefono = form.cleaned_data['telefono']
				direccion = form.cleaned_data['direccion']
				correo = form.cleaned_data['correo']
				rol = form.cleaned_data['rol']
				p = Persona()		
				p.nombres=nombres
				p.apellidos=apellidos
				p.telefono=telefono
				p.direccion=direccion
				p.correo=correo
				p.rol=rol				
				p.save() #Guardar la informacion
				info="Se  registro correctamente !!"

				
	if request.method == "GET":
		p = Persona()	
		form = addUsuarioForm(initial={
								'nombres': p.nombre,
								'apellidos': p.apellido,
								'telefono': p.telefono,
								'direccion': p.direccion,
								'correo': p.correo,
								'rol': p.correo,

			})
	ctx= {'form': form, 'personas':p}	

    	return render_to_response('editar.html',ctx,context_instance=RequestContext(request))

def consultar_u(request): 	
	
    lista = {} 
    persona = Persona.objects.all()

    lista['titulo'] = "Listado de usuarios"
    lista['Persona'] = persona

    return render(request, 'editar.html', lista, context_instance=RequestContext(request))

def buscar(request):
	lista = {}     
	error=False
	if 'q' in request.GET:
		q= request.GET['q']
		if not q:
			error= True
		else:
			persona= Persona.objects.filter(cedula=q)
			#lista['titulo'] = "Listado de usuarios"
    		#lista['Persona'] = persona
			return render(request,'editar.html',{'persona':persona,'query':q})
	
	return render(request, 'editar.html', {'error':error})

def modificar(request):
	lista = {}     
	error=False
	if 'q' in request.GET:
		q= request.GET['q']
		if not q:
			error= True
		else:
			persona= Persona.objects.filter(cedula=q)
			#lista['titulo'] = "Listado de usuarios"
    		#lista['Persona'] = persona
			return render(request,'modificar.html',{'persona':persona,'query':q})
	
	return render(request, 'modificar.html', {'error':error})


def editar_m(request):	
	error=False
	if request.method == "GET":
		if 'q' in request.GET:
			q= request.GET['q']
	
			if not q:
				error= True
			else:
				p = Persona.objects.get(cedula=q)
					
				ro = Rol.objects.filter(idrol=1)

				form = addUsuarioForm(initial={
								'cedula':p.cedula,
								'nombre': p.nombre,
								'apellido': p.apellido,
								'telefono': p.telefono,
								'direccion': p.direccion,
								'correo': p.correo,
								'fechanacimiento': p.fechanacimiento,
								

				})

				ctx= {'form': form, 'personas':p, 'rol':ro}
				
	    			return render_to_response('Modificar.html',ctx,context_instance=RequestContext(request))

	if request.method == "POST":

		form=addUsuarioForm(request.POST,request.FILES)
		if form.is_valid():
				rol = request.POST['q']
				idrol = Rol.objects.get(idrol=rol)
				#idrol = Rol.objects.get(idrol=1)
				cedula = form.cleaned_data['cedula']				
				nombre = form.cleaned_data['nombre']
				apellido = form.cleaned_data['apellido']
				telefono = form.cleaned_data['telefono']
				direccion = form.cleaned_data['direccion']
				correo = form.cleaned_data['correo']
				fechanacimiento= form.cleaned_data['fechanacimiento']
				
				p = Persona()	
				p.cedula=cedula	
				p.nombre=nombre
				p.apellido=apellido
				p.telefono=telefono
				p.direccion=direccion
				p.correo=correo
				p.fechanacimiento=fechanacimiento
				p.idrol=idrol				
				p.save() #Guardar la informacion
				info="Se  registro correctamente !!"
	form = addUsuarioForm()
	ctx = {'form': form}

    	return render_to_response('Modificar.html',ctx,context_instance=RequestContext(request))

def consultar_us(request):
	error=False
	if request.method == "GET":
		if 'q' in request.GET:
			q= request.GET['q']
			if not q:
				error= True
			else:
				persona= Persona.objects.filter(cedula=q)
			
				#lista['titulo'] = "Listado de usuarios"
    			#lista['Persona'] = persona
				return render(request,'Consultar.html',{'persona':persona,'query':q})

	if request.method == "POST":

		if 'q' in request.GET:
			q= request.GET['q']
			if not q:
				error= True
			else:
				persona= Persona.objects.get(cedula=q)
				persona.delete()

	return render(request, 'Consultar.html', {'error':error})


def eliminar_u(request):
	lista = {}     
	error=False
	if 'q' in request.GET:
		q= request.GET['q']
		if not q:
			error= True
		else:
			persona= Persona.objects.get(cedula=q)
			persona.delete()
			#lista['titulo'] = "Listado de usuarios"
    		#lista['Persona'] = persona
			return render(request,'Consultar.html',{'persona':persona,'query':q})
	
	return render(request, 'Consultar.html', {'error':error})

def ingreso_material(request):

	if request.method == "POST":
		form = addMaterialForm(request.POST)
		info = "Inicializando"
		if form.is_valid():
				dispo = request.POST['disp']
				tip= request.POST['tip']
				subc= request.POST['cat']

				iddisp = Disponibilidad.objects.get(iddisponibilidad=dispo)
				idtip= Tipolibro.objects.get(idtipolibro=tip)
				ide=Editorial.objects.get(ideditorial=1)
				idsubc= Subcategoria.objects.get(idsubcategoria=subc)
				
				#idrol=form.cleaned_data['idrol']
				codigo= form.cleaned_data['codigo']
				titulo = form.cleaned_data['titulo']
				idioma = form.cleaned_data['idioma']
				ubicacion = form.cleaned_data['ubicacion']
				fechapublicacion = form.cleaned_data['fechapublicacion']
				edicion = form.cleaned_data['edicion']
				
				l = Libro()				
				l.codigolibro=codigo
				l.edicion=edicion
				l.fechapublicacion=fechapublicacion
				l.ubicacion=ubicacion
				l.idioma=idioma
				l.titulo=titulo		
				l.iddisponibilidad=iddisp				
				l.ideditorial=ide		
				l.idtipolibro=idtip
				l.idsubcategoria=idsubc
				l.save() #Guardar la informacion
				info="Se  registro correctamente !!"
		else:
				info="datos incorrectos"
		form = addMaterialForm()
		ctx = {'form': form, 'informacion':info}
		categoria= Categoria.objects.all()
		disponible= Disponibilidad.objects.all()
		tipo=Tipolibro.objects.all()
		subca=Subcategoria.objects.all()
		ctx= {'form': form, 'categoria':categoria, 'disponible':disponible, 'tipo':tipo,'subcategoria':subca,'info':info}

    		return render_to_response('ingresomaterial.html',ctx,context_instance=RequestContext(request))

	else: #GET
		form = addMaterialForm()
		ctx = {'form': form}
		categoria= Categoria.objects.all()
		disponible= Disponibilidad.objects.all()
		tipo=Tipolibro.objects.all()
		subca=Subcategoria.objects.all()
			#lista['titulo'] = "Listado de usuarios"
    		#lista['Persona'] = persona
    		ctx= {'form': form, 'categoria':categoria, 'disponible':disponible, 'tipo':tipo,'subcategoria':subca}	

    	return render_to_response('ingresomaterial.html',ctx,context_instance=RequestContext(request))

    	#return render_to_response('ingresomaterial.html',ctx,context_instance=RequestContext(request))
	   

def modificar_material(request):
	error=False
	if request.method == "GET":
		if 'q' in request.GET:
			q= request.GET['q']
	
			if not q:
				error= True
				
			else:
				l = Libro.objects.get(codigolibro=q)				
				
				rol = Rol.objects.filter(idrol=1)
				subc=Subcategoria.objects.filter(idsubcategoria=1)
				dispo=Disponibilidad.objects.filter(iddisponibilidad=1)
				tip=Tipolibro.objects.filter(idtipolibro=1)

				form = addMaterialForm(initial={
								'codigo':l.codigolibro,
								'titulo': l.titulo,
								'idioma': l.idioma,
								'ubicacion': l.ubicacion,
								'fechapublicacion': l.fechapublicacion,
								'edicion': l.edicion,								

				})
				
				ctx= {'form': form, 'libro':l, 'rol':rol, 'subcategoria':subc,'disponible':dispo,'tipo':tip}
				
	    			return render_to_response('modificacionmaterial.html',ctx,context_instance=RequestContext(request))
	
	if request.method == "POST":
		form = addMaterialForm(request.POST,request.FILES)
		
		if form.is_valid():
				dispo = request.POST['disp']
				tip= request.POST['tip']
				subc= request.POST['cat']
				q=request.GET['q']

				iddisp = Disponibilidad.objects.get(iddisponibilidad=dispo)
				idtip= Tipolibro.objects.get(idtipolibro=tip)
				ide=Editorial.objects.get(ideditorial=1)
				idsubc= Subcategoria.objects.get(idsubcategoria=subc)
				
				#idrol=form.cleaned_data['idrol']
				codigo= form.cleaned_data['codigo']
				titulo = form.cleaned_data['titulo']
				idioma = form.cleaned_data['idioma']
				ubicacion = form.cleaned_data['ubicacion']
				fechapublicacion = form.cleaned_data['fechapublicacion']
				edicion = form.cleaned_data['edicion']
				li=Libro.objects.get(codigolibro=q)
				l = Libro()

				l.idlibro=li.idlibro
				l.codigolibro=codigo
				l.edicion=edicion
				l.fechapublicacion=fechapublicacion
				l.ubicacion=ubicacion
				l.idioma=idioma
				l.titulo=titulo		
				l.iddisponibilidad=iddisp				
				l.ideditorial=ide		
				l.idtipolibro=idtip
				l.idsubcategoria=idsubc
				l.save() #Guardar la informacion
				info="Se  registro correctamente !!"

	form = addMaterialForm()
	ctx = {'form': form}

    	return render_to_response('modificacionmaterial.html',ctx,context_instance=RequestContext(request))

def eliminar_material(request):
	error=False
	if request.method == "GET":
		if 'q' in request.GET:
			q= request.GET['q']
			if not q:
				error= True
			else:
				l= Libro.objects.filter(codigolibro=q)

			
				#lista['titulo'] = "Listado de usuarios"
    			#lista['Persona'] = persona
				return render(request,'eliminarmaterialb.html',{'libro':l,'query':q})

	if request.method == "POST":

		if 'q' in request.GET:
			q= request.GET['q']
			if not q:
				error= True
			else:
				l= Libro.objects.get(codigolibro=q)
				l.delete()

	return render(request, 'eliminarmaterialb.html', {'error':error})

# Metodo de busqueda de material

def buscar_material(request):
    query = request.GET.get('q', '')
    if query:
        qset = (Q(titulo__icontains=query) | Q(codigolibro=query))
        results = Libro.objects.filter(qset).distinct()
        #results = Libroautor.objects.filter(id_autor).distinct()
    else:
        results = []
    return render_to_response("busquedamaterial.html", {
        "results": results,
        "query": query
    })