# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class Accesos(models.Model):
    idacceso = models.AutoField(primary_key=True)
    clave = models.CharField(max_length=200)
    fechaingreso = models.DateField()
    idpersona = models.ForeignKey('Personas', db_column='idpersona')

    class Meta:
        managed = False
        db_table = 'accesos'


class Libros(models.Model):
    id_libro = models.AutoField(primary_key=True)
    titulo = models.CharField(max_length=200)
    editorial = models.CharField(max_length=200)
    edicion = models.CharField(max_length=200)
    fechapublicacion = models.DateField()
    categoria = models.CharField(max_length=200)
    autor = models.CharField(max_length=200)
    ubicacion = models.CharField(max_length=200)
    tipo = models.CharField(max_length=200)
    idioma = models.CharField(max_length=200)
    estado = models.CharField(max_length=200)
    disponibilidad = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'libros'


class Personas(models.Model):
    idpersona = models.AutoField(primary_key=True)
    nombres = models.CharField(max_length=200)
    apellidos = models.CharField(max_length=200)
    telefono = models.CharField(max_length=200, blank=True, null=True)
    direccion = models.CharField(max_length=200, blank=True, null=True)
    correo = models.CharField(max_length=200, blank=True, null=True)
    rol = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'personas'


class Prestamos(models.Model):
    idprestamo = models.AutoField(primary_key=True)
    fechaprestamo = models.DateField()
    fechadevolucion = models.DateField()
    estado = models.CharField(max_length=200)
    idpersona = models.ForeignKey(Personas, db_column='idpersona')
    id_libro = models.ForeignKey(Libros, db_column='id_libro')

    class Meta:
        managed = False
        db_table = 'prestamos'
