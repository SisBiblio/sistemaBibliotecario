# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class Acceso(models.Model):
    cedula = models.ForeignKey('Persona', db_column='cedula', primary_key=True)
    idpersona = models.CharField(max_length=200, blank=True, null=True)
    password = models.CharField(max_length=200, blank=True, null=True)
    fechaingreso = models.DateField()

    class Meta:
        managed = False
        db_table = 'acceso'


class Autor(models.Model):
    id_autor = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'autor'


class Categoria(models.Model):
    id = models.IntegerField(primary_key=True)
    tipo = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'categoria'


class Disponibilidad(models.Model):
    id = models.IntegerField(primary_key=True)
    disponibilidad = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'disponibilidad'


class Editorial(models.Model):
    id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'editorial'


class Estadolibro(models.Model):
    estado = models.IntegerField(primary_key=True)
    estado_1 = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'estadolibro'


class Estadoprestamo(models.Model):
    cedula = models.ForeignKey('Prestamo', db_column='cedula', primary_key=True)
    nombre = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'estadoprestamo'


class Libro(models.Model):
    id = models.ForeignKey(Editorial, db_column='id', primary_key=True)
    id_editoriales = models.CharField(max_length=200, blank=True, null=True)
    edicion = models.CharField(max_length=200, blank=True, null=True)
    fechapublicacion = models.CharField(max_length=200, blank=True, null=True)
    id_categoria = models.CharField(max_length=200, blank=True, null=True)
    id_autor = models.CharField(max_length=200, blank=True, null=True)
    ubicacion = models.CharField(max_length=200, blank=True, null=True)
    tipo = models.CharField(max_length=200, blank=True, null=True)
    idioma = models.CharField(max_length=200, blank=True, null=True)
    id_estado = models.CharField(max_length=200, blank=True, null=True)
    id_disponibilidad = models.CharField(max_length=200, blank=True, null=True)
    titulo = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'libro'


class Libroautor(models.Model):
    id = models.IntegerField(primary_key=True)
    id_1 = models.ForeignKey(Libro, db_column='id_1')
    id_autor = models.ForeignKey(Autor, db_column='id_autor')
    id_libro = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'libroautor'


class Persona(models.Model):
    cedula = models.IntegerField(primary_key=True)
    idrol = models.ForeignKey('Rol', db_column='idrol')
    nombre = models.CharField(max_length=100, blank=True, null=True)
    apellido = models.CharField(max_length=100, blank=True, null=True)
    telefono = models.CharField(max_length=50, blank=True, null=True)
    direccion = models.CharField(max_length=200, blank=True, null=True)
    correo = models.CharField(max_length=200, blank=True, null=True)
    fechanacimiento = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'persona'


class Prestamo(models.Model):
    cedula = models.ForeignKey(Libro, db_column='cedula', primary_key=True)
    id_persona = models.CharField(max_length=200)
    id_libro = models.CharField(max_length=200)
    fechaprestamo = models.DateField()
    fechadevolucion = models.DateField()
    idestado = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'prestamo'


class Rol(models.Model):
    idrol = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rol'


class Sanciones(models.Model):
    cedula = models.ForeignKey(Persona, db_column='cedula')
    fechasancion = models.DateField(blank=True, null=True)
    motivo = models.CharField(max_length=200, blank=True, null=True)
    diassancion = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sanciones'
